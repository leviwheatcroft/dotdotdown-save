import webpack from 'webpack'
// import MiniCssExtractPlugin from 'mini-css-extract-plugin'
// import {
//   BundleAnalyzerPlugin
// } from 'webpack-bundle-analyzer'
import debug from 'debug'

import {
  resolve
} from 'path'
import {
  get
} from 'http'

const dbg = debug('ddd-assets')

const publicPath = resolve(__dirname, 'dist')

// optimization, see: https://github.com/webpack-contrib/mini-css-extract-plugin
const compiler = webpack(
  {
    mode: 'production',
    // mode: 'development',
    entry: resolve(__dirname, 'src/component/index.js'),
    output: {
      path: publicPath,
      filename: 'component.js'
    },
    resolve: {
      extensions: ['.js', '.less', '.pug']
    },
    plugins: [
      // new MiniCssExtractPlugin({
      //   filename: '[name].[hash].css',
      //   chunkFilename: '[id].[hash].css'
      // }),
      // new BundleAnalyzerPlugin()
    ],
    module: {
      rules: [
        {
          test: /\.(c|le)ss$/,
          use: [
            // { loader: MiniCssExtractPlugin.loader },
            {
              loader: 'css-loader',
              options: {
                modules: false // better to specify what you want locally scoped
              }
            },
            { loader: 'less-loader' } // compiles Less to CSS
          ]
        },
        {
          test: /\.pug/,
          use: [
            { loader: 'pug-loader' }
          ]
        }
      ]
    }
  }
)

compiler.run((err, stats) => {
  dbg(stats)
  if (err) dbg(err)
  else if (stats.hasErrors()) dbg(stats.compilation.errors)
  // trigger browsersync reload
  else {
    dbg(`webpack looks ok: ${stats.hash}`)
  }
})
