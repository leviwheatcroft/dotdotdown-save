/* eslint-env browser */
/* global $ dotdotdown */

import styles from './styles'
import {
  get
} from 'lodash'

const {
  Component,
  Alert,
  Uri,
  registerPlugin
} = dotdotdown

export default class Save extends Component {
  constructor () {
    const layout = null
    super(layout, styles)
    $('document').ready(() => {
      try {
        const codeMirror = $('.CodeMirror')[0].CodeMirror
        this.changeGeneration = codeMirror.changeGeneration()
        window.onbeforeunload = () => {
          if (codeMirror.isClean(this.changeGeneration)) return
          // some browsers don't show this actual message, but it seems like
          // returning a string will make the browser alert something similar
          return "You should probably save your changes before you do that."
        }
      } catch (err) {
        return
      }
    })
  }
  async clickNav(event) {
    event.target.className = 'fa fa-spinner fa-spin'
    const path = window.location.pathname
    const markdown = $('.CodeMirror')[0].CodeMirror.getValue()
    try {
      await $.ajax({
        url: 'ddd/save',
        method: 'POST',
        // dataType: 'json',
        data: JSON.stringify({ path, markdown }),
        contentType: 'application/json',
        processData: false
      })
      $('.master-alert').component().addAlert({
        context: 'info',
        content: 'file saved',
        timeout: 3000
      })
    } catch (err) {
      /**
       * ajax error handling...
       * err here is a [jqXHR object](http://api.jquery.com/jQuery.ajax/#jqXHR)
       * if this error has been managed on the server, there should be a
       * `responseJSON.err.message` property. In other cases you might just
       * get an "Internal Server Error" or something.
       */
      const message = get(err, 'responseJSON.err.message')
      $('.master-alert').component().addAlert({
        context: 'danger',
        content: [
          message || 'Something has gone wrong trying to save this file.',
          'See console for details.'
        ].join('<br>'),
        dismissable: true
      })
      console.error('message: ', message)
      console.error(err)
    }

    this.changeGeneration = $('.CodeMirror')[0].CodeMirror.changeGeneration()
    setTimeout(() => event.target.className = 'fa fa-save', 500)
  }
  static load () {
    // detect whether this is a dir listing
    if (window.dirURI) return
    const component = new Save()
    const description = 'Save'
    const classes = ['fa', 'fa-save']
    const onClick = function (event) { component.clickNav(event) }
    $('nav').component().addButton(description, classes, onClick)
  }
}

registerPlugin(Save)
