/**
 * export a router
 *
 * if the plugin doesn't need a router, just export `false` a la:
 * `export default false`
 */

import express from 'express'
import {
  writeFileSync
} from 'fs'
import httpErrors from 'http-errors'
import debug from 'debug'

// eslint-disable-next-line no-unused-vars
const dbg = debug('ddd-create')

export default function getRouter ({ config, Path }) {
  const router = express.Router()
  router.post('/', function (req, res, next) {
    const path = Path.from(req.body.path)
    try {
      writeFileSync(path.asPath(), req.body.markdown, 'utf8')
    } catch (err) {
      const msg = `some error occurred trying to write: ${path.asPath()}`
      return next(httpErrors(500, msg))
    }
    res.status(201).end()
  })
  return router
}
