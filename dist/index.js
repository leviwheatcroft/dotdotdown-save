'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.default = getRouter;

var _express = require('express');

var _express2 = _interopRequireDefault(_express);

var _fs = require('fs');

var _httpErrors = require('http-errors');

var _httpErrors2 = _interopRequireDefault(_httpErrors);

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

// eslint-disable-next-line no-unused-vars
/**
 * export a router
 *
 * if the plugin doesn't need a router, just export `false` a la:
 * `export default false`
 */

var dbg = (0, _debug2.default)('ddd-create');

function getRouter(_ref) {
  var config = _ref.config,
      Path = _ref.Path;

  var router = _express2.default.Router();
  router.post('/', function (req, res, next) {
    var path = Path.from(req.body.path);
    try {
      (0, _fs.writeFileSync)(path.asPath(), req.body.markdown, 'utf8');
    } catch (err) {
      var msg = 'some error occurred trying to write: ' + path.asPath();
      return next((0, _httpErrors2.default)(500, msg));
    }
    res.status(201).end();
  });
  return router;
}